package main

import "fmt"

func main() {
	var chartype int8 = 'R'

	fmt.Printf("Code '%c' - %d\n", chartype, chartype)

	//Задание.
	//1. Вывести украинскую букву 'Ї'
	fmt.Println("Ї")
	//2. Пояснить назначение типа "rune"
	// rune — це функції, які виводять більш унікальні символи, включені в стандарт Unicode, наприклад емодзі.
}
