package main

//Импорт нескольких пакетов
import (
	"fmt"
	"math"
)

func main() {
	var defaultFloat float32
	var defaultDouble float64 = 5.5

	fmt.Println("defaultfloat       = ", defaultFloat)
	fmt.Printf("defaultDouble (%T) = %f\n\n", defaultDouble, defaultDouble)

	fmt.Println("MAX float32        = ", math.MaxFloat32)
	fmt.Println("MIN float32        = ", math.SmallestNonzeroFloat32, "\n")

	fmt.Println("MAX float64        = ", math.MaxFloat64)
	fmt.Println("MIN float64        = ", math.SmallestNonzeroFloat64, "\n")

	//Задание.
	// 1. Создайте переменные разных типов, используя краткую запись и инициализацию по-умолчанию. Результат вывести
	a := "fbtucftcu"
	fmt.Println("Type = %T", a)
	intVar := 0
	fmt.Printf("intVar = "+"Type = %T Value = %d\n", intVar, intVar)
	uintVar := uint16(0)
	fmt.Printf("intVar = "+"Type = %T Value = %d\n", uintVar, uintVar)
	flotVar := float32(0)
	fmt.Printf("intVar = "+"Type = %T Value = %d\n", flotVar, flotVar)

}
